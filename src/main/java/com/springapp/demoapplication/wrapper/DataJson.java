package com.springapp.demoapplication.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.springapp.demoapplication.entity.CategoryJson;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataJson
{

    @JsonProperty("data")
    private List<CategoryJson> categoryList = null;

    @JsonProperty("data")
    public List<CategoryJson> getCategories() {
        return categoryList;
    }

    @JsonProperty("data")
    public void setCategories(List<CategoryJson> data) {
        this.categoryList = data;
    }

}