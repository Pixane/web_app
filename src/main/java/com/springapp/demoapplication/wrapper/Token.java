package com.springapp.demoapplication.wrapper;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Token
{
    private static final int MILI_TO_SECONDS = 1000;

    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("expires_in")
    private long expiryTime;

    @JsonProperty("access_token")
    public String getAccessToken()
    {
        return accessToken;
    }

    @JsonProperty("access_token")
    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    @JsonProperty("refresh_token")
    public String getRefreshToken()
    {
        return refreshToken;
    }

    @JsonProperty("refresh_token")
    public void setRefreshToken(String refreshToken)
    {
        this.refreshToken = refreshToken;
    }

    @JsonProperty("expires_in")
    public long getExpiryTime()
    {
        return expiryTime;
    }

    @JsonProperty("expires_in")
    public void setExpiryTime(long expiresIn)
    {
        this.expiryTime = expiresIn + System.currentTimeMillis()/MILI_TO_SECONDS;
    }

    public boolean checkIfExpired()
    {
        boolean expired = true;

        if(this.expiryTime<System.currentTimeMillis()/MILI_TO_SECONDS)
        {
            expired = false;
        }

        return expired;

    }
}
