package com.springapp.demoapplication.wrapper;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonIgnoreProperties(ignoreUnknown = true)
public class SiteJson
{
    @JsonProperty("uniqueId")
    private String uniqueId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("serverParameters")
    private ServerParametersJson serverParametersJson;

    @JsonProperty("uniqueId")
    public String getUniqueId() {
        return uniqueId;
    }

    @JsonProperty("uniqueId")
    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("serverParameters")
    public ServerParametersJson getServerParametersJson() {
        return serverParametersJson;
    }

    @JsonProperty("serverParameters")
    public void setServerParametersJson(ServerParametersJson serverParametersJson) {
        this.serverParametersJson = serverParametersJson;
    }

}
