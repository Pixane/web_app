package com.springapp.demoapplication.wrapper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SiteUrl
{
    @Value("${rest.url}")
    private String baseUrl;

    @Value("${rest.username}")
    private String username;

    @Value("${rest.password}")
    private String password;

    @Value("${rest.client.id}")
    private String clientId;

    @Value("${rest.client.secret}")
    private String clientSecret;

    @Value("${rest.grant.type}")
    private String grantType;

    @Value("${rest.url.api}")
    private String restUrlApi;

    public String getUrl()
    {
        return baseUrl+"?username="+username+"&password="+password+"&client_id="+clientId+"&client_secret="+clientSecret+"&grant_type="+grantType;
    }

    public String getApiUrl()
    {
        return restUrlApi;
    }
}
