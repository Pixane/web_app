package com.springapp.demoapplication.service;

import com.springapp.demoapplication.entity.Category;
import com.springapp.demoapplication.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * Handles communication between Controllers and Repository. Does additional buisness logic when required.
 */

@Service
public class CategoryServiceImpl implements CategoryService
{
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    @Transactional
    public List<Category> getCategories()
    {
        return categoryRepository.findAll();
    }

    @Override
    @Transactional
    public void saveCategory(Category category)
    {
        if(categoryRepository.existsByNameIgnoreCase(category.getName()))
        {
            throw new DataIntegrityViolationException("Error during saving procedure.");
        }
        categoryRepository.save(category);
    }

    @Override
    @Transactional
    public Category getCategory(int id)
    {
        return categoryRepository.getOne(id);
    }

    @Override
    @Transactional
    public void removeCategory(int categoryId)
    {
        categoryRepository.deleteById(categoryId);
    }

    @Override
    @Transactional
    public void saveCategories(List<Category> categories)
    {
        List<Category> categoriesWithoutRepeats = new LinkedList<>();
        for(Category category: categories)
        {
            if(!categoryRepository.existsByNameIgnoreCase(category.getName()))
            {
                categoriesWithoutRepeats.add(category);
            }
        }
        categoryRepository.saveAll(categoriesWithoutRepeats);
    }

    @Override
    @Transactional
    public void removeAllCategories()
    {
        categoryRepository.deleteAll();
    }

    @Override
    @Transactional
    public Page<Category> findPaginatedQuery(String query, Pageable pageable)
    {
        Page<Category> categoriesPage = categoryRepository.findByNameContainingIgnoreCase(query, pageable);
        return categoriesPage;
    }
}
