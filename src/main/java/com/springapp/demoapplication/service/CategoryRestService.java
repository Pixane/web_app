package com.springapp.demoapplication.service;

import com.springapp.demoapplication.entity.Category;
import com.springapp.demoapplication.wrapper.SiteJson;

import java.util.List;

/**
 * Prototype for Rest Service.
 */
public interface CategoryRestService
{
    public void importSiteList();
    public List<Category> importCategoryList(SiteJson theSiteJson);
    public List<SiteJson> getSiteJsonList();
}
