package com.springapp.demoapplication.service;

import com.springapp.demoapplication.entity.*;
import com.springapp.demoapplication.wrapper.DataJson;
import com.springapp.demoapplication.wrapper.SiteJson;
import com.springapp.demoapplication.wrapper.SiteUrl;
import com.springapp.demoapplication.wrapper.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedList;
import java.util.List;

/**
 * Handles communication between external REST API and application. Pulls data from external file to avoid
 * hardcoding secret values. Will require higher degree of security in commercial use (better handling of passwords
 * and secrets).
 */

@Service
public class CategoryRestServiceImpl implements CategoryRestService
{

    @Autowired
    private RestTemplate restTemplate = new RestTemplate();
    private SiteUrl siteUrl;
    private Token currentToken;
    private List<SiteJson> siteJsonList;

    @Autowired
    public CategoryRestServiceImpl(SiteUrl siteUrl)
    {
        this.siteUrl = siteUrl;
        generateAccessToken();
        importSiteList();
    }

    private void refreshToken()
    {
        if(currentToken.checkIfExpired())
        {
            generateAccessToken();
        }
    }

    private void generateAccessToken()
    {
        currentToken = restTemplate.getForObject(siteUrl.getUrl(), Token.class);
    }

    public void importSiteList()
    {
        refreshToken();
        ResponseEntity<List<SiteJson>> response = restTemplate.exchange(
                siteUrl.getApiUrl()+"?access_token="+currentToken.getAccessToken(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<SiteJson>>(){});

        siteJsonList = response.getBody();
    }

    public List<Category> importCategoryList(SiteJson siteJson)
    {
        refreshToken();
        DataJson dataJson = restTemplate.getForObject(siteJson.getServerParametersJson().getFullAddress()+"/api/venue/"+
                siteJson.getUniqueId() + "/productCategory?access_token="+currentToken.getAccessToken(), DataJson.class);

        List<CategoryJson> categoriesJson = dataJson.getCategories();
        List<Category> categories = new LinkedList<>();
        for(CategoryJson categoryJson: categoriesJson)
        {
            categories.add(new Category(categoryJson.getName()));
        }
        return categories;
    }

    public List<SiteJson> getSiteJsonList()
    {
        return siteJsonList;
    }
}