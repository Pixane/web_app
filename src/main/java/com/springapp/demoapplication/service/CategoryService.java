package com.springapp.demoapplication.service;

import com.springapp.demoapplication.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Prototype for Category service.
 */

public interface CategoryService
{
    public List<Category> getCategories();
    public void saveCategory(Category category);
    public Category getCategory(int id);
    public void saveCategories(List<Category> categories);
    public void removeCategory(int categoryId);
    public void removeAllCategories();
    public Page<Category> findPaginatedQuery(String query, Pageable pageable);
}
