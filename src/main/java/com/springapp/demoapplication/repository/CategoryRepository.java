package com.springapp.demoapplication.repository;

import com.springapp.demoapplication.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Handles communication with database.
 */

public interface CategoryRepository extends JpaRepository<Category,Integer>
{
    public Page<Category> findByNameContainingIgnoreCase(String name, Pageable pageable);
    public boolean existsByNameIgnoreCase(String name);
}
