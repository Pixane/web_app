package com.springapp.demoapplication.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
/**
 * Sets /categories/list as base path when entering the website.
 */

@Controller
public class BaseController
{
    @GetMapping("/")
    public String baseRedirect()
    {
        return "redirect:/categories/list";
    }
}
