package com.springapp.demoapplication.controller;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Handles exceptions raised by either Controller, Service or Repository.
 */
@ControllerAdvice
public class ExceptionController
{
    @ExceptionHandler({DataIntegrityViolationException.class, NullPointerException.class, IllegalArgumentException.class
            , IllegalStateException.class})
    public String handleDataIntegrityViolationException(RedirectAttributes redirectAttributes, Exception exception)
    {
        if (exception.getMessage().equals("Error during saving procedure."))
        {
            redirectAttributes.addFlashAttribute("errorMessage", "Unable to add/edit element.");
        }

        return "redirect:list";
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public String handleEmptyResultDataAccessException(RedirectAttributes redirectAttributes)
    {
        redirectAttributes.addFlashAttribute("errorMessage", "Unable to remove element.");

        return "redirect:list";
    }
}