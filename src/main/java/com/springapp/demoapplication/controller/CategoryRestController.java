package com.springapp.demoapplication.controller;

import com.springapp.demoapplication.entity.Category;
import com.springapp.demoapplication.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Sends List<Category> as REST server in JSON format.
 */
@RestController
@RequestMapping("/api")
public class CategoryRestController
{
    @Autowired
    private CategoryService categoryService;

    @GetMapping("category")
    public List<Category> findAll()
    {
        return categoryService.getCategories();
    }
}
