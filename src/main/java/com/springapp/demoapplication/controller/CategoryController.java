package com.springapp.demoapplication.controller;

import com.springapp.demoapplication.entity.Category;
import com.springapp.demoapplication.wrapper.SiteJson;
import com.springapp.demoapplication.service.CategoryService;
import com.springapp.demoapplication.service.CategoryRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Handles the control part of MVC. Additionally does partial validation of inputs.
 */
@Controller
@RequestMapping("/categories")
public class CategoryController
{
    private static final String REDIRECT_LIST = "redirect:list";
    private static final String CATEGORY = "category";

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRestService categoryRestService;

    @GetMapping("/list")
    public String listCategories(Model model,
                                 @RequestParam("databaseQuery") Optional<String> databaseQuery,
                                 @RequestParam("page") Optional<Integer> page,
                                 @RequestParam("size") Optional<Integer> size)
    {
        String currentQuery = databaseQuery.orElse("");
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        categoryRestService.importSiteList();
        List<SiteJson> sites = categoryRestService.getSiteJsonList();

        Page<Category> categoryPage = categoryService.findPaginatedQuery(currentQuery,
                PageRequest.of(currentPage-1, pageSize));

        int totalPages = categoryPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        model.addAttribute(CATEGORY, new Category());
        model.addAttribute("categoryPage", categoryPage);
        model.addAttribute("siteJsonList", sites);
        return "list-customers";
    }

    @PostMapping("/save")
    public String addCategory(@ModelAttribute(CATEGORY) Category category)
    {
        String categoryName = category.getName();

        if(categoryName == null)
        {
            throw new NullPointerException("Error during saving procedure.");
        }
        else
        {
            if(categoryName.isEmpty() || categoryName.length()>255)
            {
                throw new IllegalArgumentException("Error during saving procedure.");
            }
        }
        categoryService.saveCategory(category);
        return REDIRECT_LIST;
    }

    @GetMapping("/delete")
    public String deleteCategory(@Valid int categoryId)
    {
        categoryService.removeCategory(categoryId);
        return REDIRECT_LIST;
    }

    @GetMapping("/editForm")
    public String editCategoryForm(@Valid int categoryId, Model model)
    {
        Category category = categoryService.getCategory(categoryId);
        model.addAttribute(CATEGORY, category);
        return "update-form";
    }

    @PostMapping("/import")
    public String importCategories(@Valid String siteIndex)
    {
        int siteId = Integer.valueOf(siteIndex);

        List<SiteJson> siteJsonList = categoryRestService.getSiteJsonList();
        SiteJson siteJson = siteJsonList.get(siteId);
        List<Category> categoryList = categoryRestService.importCategoryList(siteJson);
        categoryService.saveCategories(categoryList);

        return REDIRECT_LIST;
    }

    @GetMapping("/flush")
    public String flushCategories()
    {
        categoryService.removeAllCategories();
        return REDIRECT_LIST;
    }
}
