package com.springapp.demoapplication.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Helps with communication between external REST API and the web application.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryJson
{
    private int id;
    @JsonProperty("name")
    private String name;

    public CategoryJson(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public CategoryJson()
    {

    }

    public CategoryJson(String name)
    {
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName()
    {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name)
    {
        this.name = name;
    }
}
