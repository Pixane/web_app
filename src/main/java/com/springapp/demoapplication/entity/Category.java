package com.springapp.demoapplication.entity;

import javax.persistence.*;

/**
 * Basic entity which is used to communicate between view, model and database.
 */
@Entity
@Table(name="category")
public class Category
{
    @Id
    @GeneratedValue
    private int id;

    @Column(name="name", unique=true)
    private String name;

    public Category() { }

    public Category(String name)
    {
        this.name = name;
    }

    public Category(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String category)
    {
        this.name = category;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }
}
