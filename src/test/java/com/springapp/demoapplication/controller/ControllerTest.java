package com.springapp.demoapplication.controller;

import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.springapp.demoapplication.entity.Category;
import com.springapp.demoapplication.repository.CategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

/**
 * Test suites testing the basic use cases and limitations of application.
 */

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest
{
    @Autowired
    private MockMvc mockMvc;

    @Resource
    private CategoryRepository testRepository;

    @BeforeEach
    public void resetDatabase()
    {
        testRepository.deleteAll();
        testRepository.save(new Category("Jedzenie"));
        testRepository.save(new Category("Picie"));
    }

    @Test
    public void addCategory_AddNewCategory_AddedNewCategoryToDatabase() throws Exception
    {
        mockMvc.perform(post("/categories/save")
                .header("host", "localhost")
                .param("name","Napoje")
                .flashAttr("category", new Category()));


        assertTrue(testRepository.existsByNameIgnoreCase("Napoje"));
    }

    @Test
    public void addCategory_AddNewCategory_RedirectedUrlToList() throws Exception
    {
        mockMvc.perform(post("/categories/save")
                .header("host", "localhost")
                .param("name","Napoje")
                .flashAttr("category", new Category()))
                .andExpect(redirectedUrl("list"));
    }

    @Test
    public void addCategory_CategoryAlreadyInDatabase_ShowErrorMessage() throws Exception
    {
        mockMvc.perform(post("/categories/save")
                .header("host", "localhost")
                .param("name","Jedzenie")
                .flashAttr("category", new Category()))
                .andExpect(redirectedUrl("list"))
                .andExpect(flash().attribute("errorMessage", "Unable to add/edit element."));
    }

    @Test
    public void addCategory_EmptyName_ShowErrorMessage() throws Exception
    {
        mockMvc.perform(post("/categories/save")
                .header("host", "localhost")
                .param("name","")
                .flashAttr("category", new Category()))
                .andExpect(redirectedUrl("list"))
                .andExpect(flash().attribute("errorMessage", "Unable to add/edit element."));
    }

    @Test
    public void addCategory_NullObjectReceived_ShowErrorMessage() throws Exception
    {
        mockMvc.perform(post("/categories/save")
                .header("host", "localhost"))
                .andExpect(redirectedUrl("list"))
                .andExpect(flash().attribute("errorMessage", "Unable to add/edit element."));
    }

    @Test
    public void addCategory_TooLongCategory_ShowErrorMessage() throws Exception
    {
        mockMvc.perform(post("/categories/save")
                .header("host", "localhost")
                .param("name",new String(new char[256]))
                .flashAttr("category", new Category()))
                .andExpect(redirectedUrl("list"))
                .andExpect(flash().attribute("errorMessage", "Unable to add/edit element."));
    }

    @Test
    public void deleteCategory_DeleteCategory_DeletedCategoryFromDatabase() throws Exception
    {
        Category testCategory = testRepository.findAll().get(0);
        mockMvc.perform(get("/categories/delete")
                .header("host", "localhost")
                .param("categoryId", Integer.toString(testCategory.getId())));

        assertFalse(testRepository.findById(testCategory.getId()).isPresent());
    }


    @Test
    public void deleteCategory_DeleteCategory_RedirectedUrlToList() throws Exception
    {
        Category testCategory = testRepository.findAll().get(0);
        mockMvc.perform(get("/categories/delete")
                .header("host", "localhost")
                .param("categoryId", Integer.toString(testCategory.getId())))
                .andExpect(redirectedUrl("list"))
                .andExpect(flash().attribute("errorMessage", nullValue()));;
    }

    @Test
    public void deleteCategory_CategoryNotInDatabase_ShowErrorMessage() throws Exception
    {
        testRepository.deleteAll();
        mockMvc.perform(get("/categories/delete")
                .header("host", "localhost")
                .param("categoryId", "0"))
                .andExpect(redirectedUrl("list"))
                .andExpect(flash().attribute("errorMessage", "Unable to remove element."));
    }

    @Test
    public void listCategories_ListCategories_LoadedViewAndAttributes() throws Exception
    {
        mockMvc.perform(get("/categories/list")
                .header("host","localhost"))
                .andExpect(status().isOk())
                .andExpect(view().name("list-customers"))
                .andExpect(flash().attribute("errorMessage", nullValue()));;
    }

    @Test
    public void listCategories_QueryCategories_ListedQueriedCategories() throws Exception
    {
        mockMvc.perform(get("/categories/list")
                .header("host","localhost")
                .flashAttr("databaseQuery", "Jedzenie"))
                .andExpect(status().isOk())
                .andExpect(view().name("list-customers"))
                .andExpect(flash().attribute("errorMessage", nullValue()));;
    }

    @Test
    public void listCategories_EmptyQueryCategories_ListedAllCategories() throws Exception
    {
        mockMvc.perform(get("/categories/list")
                .header("host","localhost")
                .flashAttr("databaseQuery", ""))
                .andExpect(status().isOk())
                .andExpect(view().name("list-customers"))
                .andExpect(flash().attribute("errorMessage", nullValue()));;
    }

    @Test
    public void editCategoryForm_GetEditCategoryForm_ReceivedEditCategoryForm() throws Exception
    {
        Category testCategory = testRepository.findAll().get(0);
        mockMvc.perform(get("/categories/editForm")
                .header("host","localhost")
                .param("categoryId", Integer.toString(testCategory.getId())))
                .andExpect(status().isOk())
                .andExpect(view().name("update-form"));
    }


    @Test void importCategories_ImportCategories_NewCategoriesImported() throws Exception
    {
        mockMvc.perform(post("/categories/import")
                .header("host","localhost")
                .param("siteIndex", "0"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("list"))
                .andExpect(flash().attribute("errorMessage", nullValue()));
    }

    @Test void importCategories_ImportCategoriesWithDoubles_NewCategoriesImported() throws Exception
    {

        mockMvc.perform(post("/categories/import")
                .header("host","localhost")
                .param("siteIndex", "0"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("list"))
                .andExpect(flash().attribute("errorMessage", nullValue()));
    }

    @Test void baseRedirect_RedirectToCategoryList_ListedAllCategories() throws Exception
    {
        Category category = new Category("Alkohole");
        testRepository.save(category);
        mockMvc.perform(get("/")
                .header("host","localhost"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/categories/list"))
                .andExpect(flash().attribute("errorMessage", nullValue()));
    }
}
